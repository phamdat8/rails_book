
class UsersController < ApplicationController
  def new
    @user = User.new
    @link = 'sign_up'
  end

  def create
    @user = User.new user_params
    if @user.save
      UserMailer.account_activation(@user).deliver_now

      flash[:success] = 'You are signed up successfully, please check your email to active your account !!!'
      redirect_to root_path
    else
      flash.now[:warning] = 'Something went wrong'
      render :new
    end
  end
  def active_account
    #tim kiem email
    @user = User.find_by email: params[:email]
      #kiem tra email ton tai va so sanh token voi activation_token trong database
      if @user&.authenticated?(:activation, params[:token]) && @user&.active_account!
        flash[:success] = 'you are actived'
        redirect_to root_path
      else
        flash[:alert] = 'active link is invalid'
      end
  end

  #check password

  def forgot_password
    @user = User.new
  end

  def check_email
    @user = User.find_by email: params[:user][:email]
    if @user
      @user.activation_token = SecureRandom.base64(20)
      @user.activation_digest = BCrypt::Password.create(@user.activation_token)
      @user.update_attributes activation_digest: @user.activation_digest, activation_token: @user.activation_token
      UserMailer.reset_password(@user).deliver_now
      flash[:success] = 'Please check your email to reset password'
      @user = User.new
      render :new
    else
      flash[:warning] = 'Your account does not exist'
      @user = User.new
      render :forgot_password
    end
  end
  #reset password
  def reset_password
    @user = User.new
    @user.email = params[:email]
  end

  def change_password
    @user = User.find_by email: params[:user][:email]
      if @user&.authenticated?(:activation, params[:token])
        @user.update_attributes password: params[:user][:password], activation_digest: nil
        flash[:success] = 'Change password successfully'

        redirect_to root_path
      else
        redirect_to root_path
      end
  end

  def profile
  end

  def edit
    @user = User.new
  end


  private

  def user_params
    params.require(:user).permit :name, :email, :password, :password_confirmation
  end
end
