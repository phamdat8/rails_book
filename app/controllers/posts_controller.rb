class PostsController < ApplicationController
  def index

    @posts = Micropost.paginate(:page => params[:page], :per_page => 2)
  end

  def new
    @post = Micropost.new
  end

  def show
    @post = Micropost.find(params[:id])
    @comments = @post.comments
    @comment= Comment.new
  end

  def create
    @post = Micropost.new
    @post.content = params[:micropost][:content]
    @post.user_id = current_user.id
    if @post.save

      redirect_to posts_path
    else
      render :new
    end
  end
end
