class CommentsController < ApplicationController
  def create
    @comment = Comment.new
    @comment.user_id = params[:comment][:user_id]
    @comment.micropost_id = params[:comment][:micropost_id]
    @comment.content = params[:comment][:content]

    if @comment.save
      redirect_to post_path(@comment.micropost)
    else

    end
  end
end
