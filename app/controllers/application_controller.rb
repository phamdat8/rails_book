class ApplicationController < ActionController::Base
  include SessionsHelper

  before_action :check_session
  after_action :check_session

  private

  def check_session

  end
end
