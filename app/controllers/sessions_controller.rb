class SessionsController < ApplicationController
  def new
    @user = User.new
    @link ='sign_in'
  end

  def create
    @user = User.find_by email: params[:user][:email]

    if @user&.authenticated?(:password, params[:user][:password])
      if @user.activated?
        login(@user)
        params[:user][:remember_me] == '1' ? remember(@user) : forgot(@user)
        flash[:success] = 'You are signed in successfully!'
      else
        flash[:warning] = 'Your account is not actived. Please check your email and active'
      end
      redirect_to root_path
    else
      @user = User.new user_params
      flash.now[:warning] = 'Wrong email or password. please try again!'
      render :new
    end
  end

  def destroy
    cookies.delete :user_id
    cookies.delete :remember_token
    current_user.update_attribute :remember_digest, nil
    session[:user_id] = nil
    flash[:sucess] = 'You are signed out successfully'
    redirect_to root_path
  end

  private

  def user_params
    params.require(:user).permit :email, :password
  end
end
