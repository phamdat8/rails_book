require 'bcrypt'
class User < ApplicationRecord
  include BCrypt
  has_secure_password
  has_many :microposts
  has_many :comments
  before_save {self.email.downcase!}
  before_create :generate_activation_token
  attr_accessor :activation_token
  attr_accessor :remember_token


  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  # Validation
  validates :name, presence: true
  validates :email, uniqueness: true, format: { with: VALID_EMAIL_REGEX, message: 'The email value is invalid' }
  def generate_activation_token
    self.activation_token = SecureRandom.urlsafe_base64
    self.activation_digest = Password.create(self.activation_token)
  end

  def remember
    self.remember_token = SecureRandom.urlsafe_base64
    self.update_attribute :remember_digest, Password.create(self.remember_token)
  end

  def forgot
    self.update_attribute :remember_digest, nil
  end

  def authenticated?(attr,token)
    digest = send("#{attr}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def active_account!
    update_attributes activated: true, activated_at: Time.now, activation_digest: nil
  end



end
