class Micropost < ApplicationRecord
  belongs_to :user
  has_many :comments
  

  validates :content, presence: true, length: {minimum: 140}

  default_scope -> { order(created_at: :desc) }
end
