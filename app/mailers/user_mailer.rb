class UserMailer < ApplicationMailer
  def account_activation(user)
    @user = user
    mail to: @user.email, subject: 'please active your acccount'
  end

  def reset_password(user)
    @user = user
    mail to: @user.email, subject: 'click this link to reset your password'
  end
end
