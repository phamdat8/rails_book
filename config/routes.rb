Rails.application.routes.draw do
  root to: 'static_pages#home'
  post '/users/sign_in' => 'sessions#create'
  get '/users/sign_in' => 'sessions#new', as: :sign_in

  post '/users/sign_up' => 'users#create'
  get '/users/sign_up' => 'users#new', as: :sign_up

  delete '/users/sign_out' => 'sessions#destroy', as: :sign_out
  get 'active_account/:token' => 'users#active_account', as: :active_account

  get '/users/forgot_password' => 'users#forgot_password'
  post '/users/forgot_password' => 'users#check_email'

  get 'reset_password/:token' => 'users#reset_password', as: :reset_password
  post 'reset_password/:token' => 'users#change_password'

  get '/users/profile' => 'users#profile'

  get 'users/profile/edit' =>'users#edit'
  post 'users/profile/edit' =>'users#edit'

  resources :posts

  resources :comments
end
